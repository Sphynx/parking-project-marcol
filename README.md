# Parking System Project Marcol

As a backend engineer, you've been asked to design and implement a Parking System (REST API) with the following requirements:

* Parking type:
    - 2 entries and 2 exits.
    - Different types of vehicles that can be parked, separate spots for each type of vehicle - 30 cars, 10 busses and 20 motorcycle.

* Conditions:
    - A vehicle can enter the parking only if it has available spots for allowed vehicle types
    - In case of missing spots for specific type of vehicle an error must be returned.
    - On successful entry, return a response with the quantity of the vehicles of specific type.
    - If a vehicle type different than the allowed types, tries to enter the parking, an error should be thrown.

You've been given full decisional power on which HTTP verb and status code to use and how to structure the endpoints, but the API must have the following endpoints:
* Entering the parking. It should accept parameters for vehicle type and entry number.
* Leaving the parking. It should accept parameters for vehicle type and exit number.
* List of all available and unavailable parking spots in each level.

Please provide a production-ready code using the following technologies:
* Spring Boot 2 for REST services.
* Spring Data + Hibernate for persistence.
* JUnit for testing.
* SQL Server/In-memory database for data storage. (Hint: You can use https://hub.docker.com/_/microsoft-mssql-server docker image)
* Maven
* The code must be written in Java 8+.
* Logging - Logback or other framework
* Assume that we can have lots of concurrent requests

